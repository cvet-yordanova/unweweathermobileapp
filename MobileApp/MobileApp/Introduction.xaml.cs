﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Introduction : ContentPage
    {
        public Introduction()
        {
            InitializeComponent();
            if (Device.RuntimePlatform == Device.Android)
            {
                Label1.Text = "Running on Android";
            }
            if (Device.RuntimePlatform == Device.iOS)
            {
                Label1.Text = "Running on iOS";
            }
        }
    }
}