﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp
{
    public class Sys
    {
        [JsonProperty("type")]
        public long Pressure { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("message")]
        public long Message { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("sunrise")]

        public long Sunrise { get; set; }
        [JsonProperty("sunset")]
        public long Sunset { get; set; }

    }
}
