﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp
{
    public class Weather
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("main")]
        public string Visibility { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }
    }
}
