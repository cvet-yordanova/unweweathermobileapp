﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp
{
    public class Wind
    {
        [JsonProperty("speed")]
        public double Speed { get; set; }
        [JsonProperty("deg")]
        public long Deg { get; set; }

    }
}
