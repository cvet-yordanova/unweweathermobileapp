﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MobileApp
{
    class OpenWeatherService
    {
        HttpClient _client;

        public OpenWeatherService()
        {
            _client = new HttpClient();

        }
        public async Task<WeatherData> GetWeatherData(string query)
        {
            WeatherData watherData = null;

            try
            {
                var response = await _client.GetAsync(query);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    watherData = JsonConvert.DeserializeObject<WeatherData>(content);
                }
            } catch (Exception ex)
            {
                Debug.WriteLine("\t\tERROR {0}", ex.Message);
            }

            return watherData;
        }

        public async Task<WeatherForecast> GetWeatherForecastData(string query)
        {
            WeatherForecast weatherData = null;

            try
            {
                var response = await _client.GetAsync(query);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    weatherData = JsonConvert.DeserializeObject<WeatherForecast>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\t\tERROR {0}", ex.Message);
            }

            return weatherData;
        }
    }
}
