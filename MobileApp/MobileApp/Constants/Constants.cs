﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp
{
    class Constants
    {
        public static string OpenWeatherMapEndpoint = "https://api.openweathermap.org/data/2.5/weather";
        public static string OpenWeatherForecastEndpoint = "https://api.openweathermap.org/data/2.5/forecast";
       // public static string OpenWeatherIcon = "http://openweathermap.org/img/wn/10d@2x.png";
        public static string OpenWeatherIcon = "https://openweathermap.org/img/wn/";
        public static string OpenWeatherMapAPIKey = "2c24436d9d9a44bc6d9eae99d7835bb9";

    }
}
