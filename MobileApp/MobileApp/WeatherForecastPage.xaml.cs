﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeatherForecastPage : ContentPage
    {
        OpenWeatherService _openWeatherService;
        public WeatherForecastPage()
        {
            InitializeComponent();
            _openWeatherService = new OpenWeatherService();
            GetWeatherForecastWithGeoLoaction();
        }

        async void GetWeatherForecastWithGeoLoaction()
        {
            var location = await Geolocation.GetLocationAsync();
            if (location != null)
            {
                double lat = location.Latitude;
                double lon = location.Longitude;

                WeatherForecast weatherData = await _openWeatherService.GetWeatherForecastData(GenerateRequestUriGeo(Constants.OpenWeatherForecastEndpoint, lat, lon));
                BindingContext = weatherData;
            }

        }

        string GenerateRequestUriGeo(string endpoint, double lati, double longt)
        {
            string requestUri = endpoint;
            requestUri += $"?lat={lati}";
            requestUri += $"&lon={longt}";
            requestUri += $"&units=imperial";
            requestUri += $"&APPID={Constants.OpenWeatherMapAPIKey}";
            return requestUri;
        }

        string GenerateRequestUri(string endpoint)
        {
            string requestUri = endpoint;
            requestUri += $"?q={_cityEntry.Text}";
            requestUri += "&units=metric";
            requestUri += $"&APPID={Constants.OpenWeatherMapAPIKey}";
            return requestUri;
        }

        public async void OnGetWeatherButtonClicked(object sender, EventArgs args)
        {
            if (!string.IsNullOrWhiteSpace(_cityEntry.Text))
            {
                WeatherForecast weatherData = await _openWeatherService.GetWeatherForecastData(GenerateRequestUri(Constants.OpenWeatherForecastEndpoint));
                
                BindingContext = weatherData;
            }

        }

    }
}