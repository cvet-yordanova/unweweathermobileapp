﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;


namespace MobileApp
{
    public partial class MainPage : MasterDetailPage
    {

        OpenWeatherService _openWeatherService;
        public MainPage()
        {
            InitializeComponent();


            Detail.BackgroundColor = Color.Coral;



            /*  _openWeatherService = new OpenWeatherService();

              if(Device.RuntimePlatform == Device.Android)
              {
                  Label1.Text = "Running on Android";
              }
              if(Device.RuntimePlatform == Device.iOS)
              {
                  Label1.Text = "Running on iOS";
              }

              GetWeatherWithGeoLoaction();*/
        }

        /*   async void GetWeatherWithGeoLoaction()
           {
               var location = await Geolocation.GetLocationAsync();
               if(location != null)
               {
                   double lat = location.Latitude;
                   double lon = location.Longitude;

                   WeatherData weatherData = await _openWeatherService.GetWeatherData(GenerateRequestUriGeo(Constants.OpenWeatherMapEndpoint, lat, lon));
                   BindingContext = weatherData;
               }

           }*/

        /*    string GenerateRequestUriGeo(string endpoint, double lati, double longt)
            {
                string requestUri = endpoint;
                requestUri += $"?lat={lati}";
                requestUri += $"&lon={longt}";
                requestUri += $"&units=imperial";
                requestUri += $"&APPID={Constants.OpenWeatherMapAPIKey}";
                return requestUri;
            }*/

        /*   string GenerateRequestUri(string endpoint)
           {
               string requestUri = endpoint;
               requestUri += $"?q={_cityEntry.Text}";
               requestUri += "&units=metric";
               requestUri += $"&APPID={Constants.OpenWeatherMapAPIKey}";
               return requestUri;
           }*/
        /*
                public async void OnGetWeatherButtonClicked(object sender, EventArgs args)
                {
                    if (!string.IsNullOrWhiteSpace(_cityEntry.Text))
                    {
                        WeatherData weatherData = await _openWeatherService.GetWeatherData(GenerateRequestUri(Constants.OpenWeatherMapEndpoint));
                        BindingContext = weatherData;
                    }

                }*/

      /*  public void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedItem = e.SelectedItem as String;
            if (selectedItem == "Weather")
            {
                Detail = new NavigationPage(new WeatherPage());
            }
            if (selectedItem == "About")
            {
                Detail = new NavigationPage(new Introduction());
            }
            if (selectedItem == "5/3 Day Weather Forecast")
            {
                Detail = new NavigationPage(new WeatherForecastPage());
            }

            IsPresented = false;
        }*/


        public void Button_Weather_Clicked(object sender, EventArgs args)
        {
            Detail = new NavigationPage(new WeatherPage());
            IsPresented = false;

        }

        public void Button_Weather_Forecast_Clicked(object sender, EventArgs args)
        {
            Detail = new NavigationPage(new WeatherForecastPage());
            IsPresented = false;

        }

        public void About(object sender, EventArgs args)
        {
            Detail = new NavigationPage(new Introduction());
            IsPresented = false;
        }

    }
}
